package MyApp::Controller::Pets;

use v5.34;
use Mojo::Base 'Mojolicious::Controller', -signatures;

sub index {
    my $self = shift->openapi->valid_input or return;

    $self->render(json => {
        foo => $self->every_param('foo'),
    });
}

1;
