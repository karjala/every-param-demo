package MyApp;
use Mojo::Base 'Mojolicious', -signatures;

use Mojo::JSON 'false';

# This method will run once at server start
sub startup ($self) {

  # Load configuration from config file
  my $config = $self->plugin('NotYAMLConfig');

  # Configure the application
  $self->secrets($config->{secrets});

  $self->plugin(OpenAPI => {
      spec => {
          swagger => '2.0',
          info    => {
              title   => 'foo',
              version => '1',
          },
          paths   => {
              '/pets' => {
                  get => {
                      produces => ['application/json'],
                      'x-mojo-to' => 'pets#index',
                      parameters => [
                          {
                              in       => 'query',
                              name     => 'foo',
                              required => false,
                              type     => 'array',
                              items    => {
                                  type => 'string',
                              },
                          },
                      ],
                      responses => {
                          '200' => {
                              description => 'hi',
                          },
                      }
                  },
              }
          },
      },
  });
}

1;
